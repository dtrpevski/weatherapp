//
//  NetworkService+Request.swift
//  WeaherApp
//
//  Created by Darko Trpevski on 25.9.21.
//

import UIKit

// MARK: - EndPoint URL's
extension NetworkService {
    
    // MARK: - Build url request
    func request(for type: RequestType, parameters: [String : String]) -> URLRequest? {
        switch type {
        case .today:
            guard let url = url(for: .today, parameters: parameters) else { return nil }
            var request = URLRequest(url: url, timeoutInterval: Double.infinity)
            request.addValue(Configuration.Authorization.API_HOST, forHTTPHeaderField: "x-rapidapi-host")
            request.addValue(Configuration.Authorization.API_KEY, forHTTPHeaderField: "x-rapidapi-key")
            return request
        case .forecast:
            guard let url = url(for: .forecast, parameters: parameters) else { return nil }
            var request = URLRequest(url: url, timeoutInterval: Double.infinity)
            request.addValue(Configuration.Authorization.API_HOST, forHTTPHeaderField: "x-rapidapi-host")
            request.addValue(Configuration.Authorization.API_KEY, forHTTPHeaderField: "x-rapidapi-key")
            return request
        }

    }
    
    // MARK: - Build URL
    func url(for type: RequestType, parameters: [String : String]) -> URL? {
        
        var url: URL?
        switch type {
        case .today:
            url = URL(string: "\(Configuration.URLs.BASE_URL)weather")
        case .forecast:
            url = URL(string: "\(Configuration.URLs.BASE_URL)forecast")
        }
        
        url = url?.appendingQueryParameters(parameters)
        guard let requestURL = url else { return nil }
        return requestURL
    }
}

enum RequestType {
    case today
    case forecast
}

protocol URLQueryParameterStringConvertible {
    var queryParameters: String {get}
}

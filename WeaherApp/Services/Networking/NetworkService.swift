//
//  NetworkService.swift
//  WeaherApp
//
//  Created by Darko Trpevski on 25.9.21.
//

import Foundation
import Combine

class NetworkService {

    // MARK: - Singlethon object
    public static let shared : NetworkService = NetworkService()
    // MARK: - Subscribers
    private var subscribers = Set<AnyCancellable>()
    
    // MARK: - Generic get call
    func fetchItems<T: Decodable>(type: RequestType, parameters: [String : String], completition: @escaping (Result<T, Error>) ->Void) {
       
        if parameters.count == 0 { return }
        var urlRequest: URLRequest?
        switch type {
        case .today:
            urlRequest = request(for: .today, parameters: parameters)
        case .forecast:
            urlRequest = request(for: .forecast, parameters: parameters)
        }
        
        guard let request = urlRequest else { return }
        
        URLSession.shared.dataTaskPublisher(for: request)
            .map{ $0.data }
            .decode(type: T.self, decoder: JSONDecoder())
            .sink { (resultCompletition) in
                switch (resultCompletition) {
                case .finished:
                    break
                case .failure(let error):
                    completition(.failure(error))
                }
            }receiveValue: { (resultArray) in
                completition(.success(resultArray))
            }.store(in: &subscribers)
    }
}

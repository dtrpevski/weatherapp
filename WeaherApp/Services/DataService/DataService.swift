//
//  DataService.swift
//  WeaherApp
//
//  Created by Darko Trpevski on 26.9.21.
//

import Foundation
import FirebaseDatabase

class DataService {

    // MARK: - Vars
    public static let shared : DataService = DataService()
    private let database = Database.database().reference()
    
    // MARK: - Add data to Firebase
    func addValue(temperature: String, city: String) {
        let udid = UIDevice.current.identifierForVendor!.uuidString
        let values = ["device": udid, "temperature": temperature, "city": city]
        database.child(udid).setValue(values)
    }
}

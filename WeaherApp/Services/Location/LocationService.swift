//
//  LocationService.swift
//  WeaherApp
//
//  Created by Darko Trpevski on 25.9.21.
//

import Foundation
import CoreLocation

class LocationService: NSObject, CLLocationManagerDelegate {

    // MARK: - vars
    static let shared = LocationService()
    let locationManager: CLLocationManager
    var currentLocation: CLLocationCoordinate2D?
    var delegate: LocationServiceDelegate?
    
    // MARK: - Init
    override init() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        super.init()
        locationManager.delegate = self
    }

    // MARK: - Start/Stop
    func start() {
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }

    func stop() {
        locationManager.stopUpdatingLocation()
    }
    
    // MARK: - LM delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let mostRecentLocation = locations.last else {
            return
        }
        
        self.currentLocation = mostRecentLocation.coordinate
        self.delegate?.didReceiveLocationUpdate(location: mostRecentLocation, error: nil)
 
    }

    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.startUpdatingLocation()
    }

    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if checkLocationServiceAuthorization() == false {
            locationManager.startUpdatingLocation()
        }
    }

    // MARK: - Check LS authorization
    func checkLocationServiceAuthorization() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch locationManager.authorizationStatus {
                case .notDetermined, .restricted, .denied:
                    return false
                case .authorizedAlways, .authorizedWhenInUse:
                    return true
                @unknown default:
                    return false
            }
        }else{
            return false
        }
    }
}

// MARK: - Update view models when location will be changed
protocol LocationServiceDelegate {
    func didReceiveLocationUpdate(location: CLLocation?, error: Error?)
}

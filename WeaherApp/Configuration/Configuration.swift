//
//  Configuration.swift
//  WeaherApp
//
//  Created by Darko Trpevski on 25.9.21.
//

import Foundation

struct Configuration {
    
    public struct URLs {
        static let BASE_URL = "https://community-open-weather-map.p.rapidapi.com/"
    }
    
    public struct Authorization {
        static let API_HOST = "community-open-weather-map.p.rapidapi.com"
        static let API_KEY = "9963773a10msh6e7689787f1bf63p1c9e36jsn34ceb6d75982"
    }

    public struct Common {
        static let timer = 0.5 * 60 // on each 30s
    }
}

//
//  DayWeatherCell.swift
//  WeaherApp
//
//  Created by Darko Trpevski on 26.9.21.
//

import UIKit

class DayWeatherCell: UITableViewCell {

    private lazy var weatherImageView: UIImageView = {
        let weatherImage = UIImageView()
        weatherImage.translatesAutoresizingMaskIntoConstraints = false
        return weatherImage
    }()
    
    private lazy var temperatureLabel: UILabel = {
        let temperatureValueLabel = UILabel()
        temperatureValueLabel.translatesAutoresizingMaskIntoConstraints = false
        temperatureValueLabel.textAlignment = .center
        temperatureValueLabel.font = UIFont.systemFont(ofSize: 50)
        temperatureValueLabel.textColor = .systemBlue
        return temperatureValueLabel
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let descriptionValueLabel = UILabel()
        descriptionValueLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionValueLabel.numberOfLines = 2
        return descriptionValueLabel
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(model: ForecastList) {
        configureLayout()
        configureConstraints()
        bind(model: model)
    }
    
    func configureLayout() {
        addSubview(weatherImageView)
        addSubview(temperatureLabel)
        addSubview(descriptionLabel)
    }
    
    func configureConstraints() {
        weatherImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        weatherImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20).isActive = true
        weatherImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        weatherImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        temperatureLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        temperatureLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        temperatureLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        temperatureLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        descriptionLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        descriptionLabel.leftAnchor.constraint(equalTo: weatherImageView.rightAnchor, constant: 10).isActive = true
        descriptionLabel.rightAnchor.constraint(equalTo: temperatureLabel.leftAnchor, constant: 10).isActive = true
        descriptionLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    func bind(model: ForecastList) {
        weatherImageView.loadFroomURL(model.weatherImageURL(), placeHolder: nil)
        temperatureLabel.text = model.temperature()
        descriptionLabel.text = model.weatherDetails()
    }
}

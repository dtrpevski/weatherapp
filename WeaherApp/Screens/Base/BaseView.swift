//
//  BaseView.swift
//  WeaherApp
//
//  Created by Darko Trpevski on 26.9.21.
//

import UIKit
import NVActivityIndicatorView
import Reachability
import CoreLocation

class BaseView: UIViewController {

    // MARK: - Vars
    var activityIndicator : NVActivityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    let reachability = try! Reachability()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(activityIndicator)
        view.bringSubviewToFront(activityIndicator)
        configureActivityIndicator()
        configureReachability()
    }
    
    func configureActivityIndicator() {
        activityIndicator.center = self.view.center
        activityIndicator.color = .systemYellow
    }
    
    func configureReachability() {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
            do{
              try reachability.startNotifier()
            }catch{
              print("could not start reachability notifier")
            }
    }
    
    @objc func reachabilityChanged(note: Notification) {}
    
    func locationServiceStatus(){
        if LocationService.shared.checkLocationServiceAuthorization() == false {
            stopActivityIndicator()
            showAlert(text: "Location serivce not avaliable")
        }
    }
    
    func startActivityIndicator() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
        }
    }
    
    func stopActivityIndicator() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
    
    func showAlert(text: String) {
        
        let alert = UIAlertController(title: "Error", message:text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            
        }))
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
}

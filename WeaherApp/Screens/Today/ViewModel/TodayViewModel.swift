//  Created Darko Trpevski on 24.9.21.
//  Copyright © Darko Trpevski. All rights reserved.
//
//  Template generated by Darko Trpevski
//  Component: ViewModel -

import UIKit
import Combine
import CoreLocation

class TodayViewModel {
    
    // MARK: - Variables
    var modelSubject = PassthroughSubject<Today, Error>()
    var timer: Timer?
    var urlParams: [String:String] = [:]
    
    // MARK: - Init
    init() {
        LocationService.shared.delegate = self
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: Configuration.Common.timer, target: self, selector: #selector(fetch), userInfo: nil, repeats: true)
        LocationService.shared.start()
    }
    
    // MARK: - Get data from api
    @objc func fetch() {
        if LocationService.shared.checkLocationServiceAuthorization() {
            NetworkService.shared.fetchItems(type: .today, parameters: urlParams) { [weak self] (result: Result<Today, Error>) in
                switch result {
                case let .success(results):
                    self?.modelSubject.send(results)
                case .failure: break
                }
                
            }
        }
    }
}

// MARK: - LocationService delegate
extension TodayViewModel: LocationServiceDelegate {
    func didReceiveLocationUpdate(location: CLLocation?, error: Error?) {
        guard let lat = location?.coordinate.latitude else { return }
        guard let lon = location?.coordinate.longitude else { return }
        urlParams = ["lat": "\(lat)", "lon": "\(lon)", "lang": "en", "units": "metric"]
        fetch()
    }

}

//
//  Int+Extension.swift
//  WeaherApp
//
//  Created by Darko Trpevski on 26.9.21.
//


import Foundation

extension Int {
    func toDate() -> Date {
        return Date(timeIntervalSince1970: Double(self))
    }
}

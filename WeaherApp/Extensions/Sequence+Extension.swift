//
//  Sequence+Extension.swift
//  WeaherApp
//
//  Created by Darko Trpevski on 26.9.21.
//

extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var seen: [Iterator.Element: Bool] = [:]
        return self.filter { seen.updateValue(true, forKey: $0) == nil }
    }
}

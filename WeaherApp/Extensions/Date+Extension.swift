//
//  Date+Extension.swift
//  WeaherApp
//
//  Created by Darko Trpevski on 26.9.21.
//

import Foundation

extension Date {
    
    func toString(withFormat format: String = "EEEE ، d MMMM yyyy") -> String {

            let dateFormatter = DateFormatter()
            dateFormatter.calendar = Calendar(identifier: .gregorian)
            dateFormatter.dateFormat = format
            let string = dateFormatter.string(from: self)

            return string
    }

}

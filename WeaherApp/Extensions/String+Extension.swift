//
//  String+Extension.swift
//  WeaherApp
//
//  Created by Darko Trpevski on 26.9.21.
//

import Foundation

extension String {
    
    func dateOnly() -> String {
        return String(self.split(separator: " ").first ?? "")
    }

    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss")-> Date?{

        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)

        return date

    }
}
